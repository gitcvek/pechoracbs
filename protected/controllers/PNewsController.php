<?php

Yii::import('ygin.modules.news.controllers.NewsController');

//$a;

class PNewsController extends NewsController
{

    public function actionIndex($idCategory = null)
    {
        $criteria = new CDbCriteria();
        $criteria->scopes = array('last');
        $criteria->condition = 'date<' . time();
        $criteria->order = 'date DESC';

        $newsModule = $this->getModule();
        $category = null;
        $categories = array();
        //Если включено отображение категорий
        if ($newsModule->showCategories) {
            if ($idCategory !== null && $category = $this->loadModelOr404('NewsCategory', $idCategory)) {
                $criteria->compare('t.id_news_category', $idCategory);
            }
            $categories = NewsCategory::model()->findAll(array('order' => 'seq'));
        }

        if (HU::get('date')) {

            $date = explode('-', HU::get('date'));

            $dateStart = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
            $dateEnd = mktime(23, 59, 59, $date[1], $date[2], $date[0]);

            $criteria->addBetweenCondition('event_date', $dateStart, $dateEnd);
        }


        $count = News::model()->count($criteria);

       /* $pages = new CPagination($count);
        $pages->pageSize = $newsModule->itemsCountPerPage;
        $pages->applyLimit($criteria);*/


        $news = News::model();

        if (strpos(Yii::app()->getRequest()->getPathInfo(), '/archived') === false) {
            $news->actual();
        } else {
            $news->archived();
        }
        $news = $news->findAll();

        if (!HU::get('date')) {

            //добавляем всё из объекта книги
            $books = Book::model()->findAll();

            foreach ($books as $book) {
                $newsb = BaseActiveRecord::newModel('PNews');
                $newsb->title = $book->title;
                $newsb->date = $book->date;
                $newsb->short = $book->short;
                $newsb->photo = $book->image;
                $newsb->id_news = '/book/' . $book->id_book;
                array_push($news, $newsb);
            }

        }

        usort($news, array($this,'sortNews'));

        $dataProvider = new CArrayDataProvider($news, array(
            'id'=>'news',
            'pagination'=>array(
                'pageSize'=>$newsModule->itemsCountPerPage,
            ),
        ));


        /*
        //добавляем всё из меню про-чтение и виртуальные выставки
        $cr = new CDbCriteria();
        //$cr->condition = array('id_parent IN (172, 174)');
        $cr->addInCondition('id_parent',array(172, 174));
        $menu = Menu::model()->findAll($cr);
        foreach ($menu as $val) {
            $model = BaseActiveRecord::newModel('PNews');
            $model->title   = $val->caption;
            //$model->date    = $val->date;
            //$model->short   = $val->short;
            $model->short   = $val->name;
            $model->photo   = $val->image;
            $model->id_news = $val->alias;
            array_push($news, $model);
        }
        */
        /*
        echo '<pre>';
        print_r($news);
        echo '</pre>';die();
        */

        $this->render('/list_news', array(
            //'news' => $news,  // список новостей
           // 'pages' => $pages,  // пагинатор
            'dataProvider' => $dataProvider,
            'category' => $category,  // текущая категория
            'categories' => $categories,  // все категории
        ));
    }

    private function sortNews($news1, $news2) {//сортирует новости по дате

        if ($news1->date == $news2->date) {
            return 0;
        }
        return ($news1->date > $news2->date) ? -1 : 1;
    }

}


?>
