<?php

class AjaxController  extends Controller {

    function actionEventData() {
        $eId = (int)Yii::app()->request->getParam('id', null);
        $event = HistoricalEvents::model()->active()->findByPk($eId);
        if($event && $event->description) {
            echo CHtml::encode($event->description);
        }
        Yii::app()->end();
    }

    function actionEvents() {
        header('Content-type: application/json');

        $eventDate = (int)Yii::app()->request->getParam('event_date', null);
        $hEvents = HistoricalEvents::model()->active()->findAll(array(
            'condition' => 'event_date IS NOT NULL AND event_date = :eventDate',
            'params' => array(':eventDate' => $eventDate),
            'order' => 'event_date ASC',
        ));

        $events = array();
        foreach ($hEvents as $event) {
            /*
            $events []= array($event->event_date => array(
                'title' => $event->title,
                'desc' => $event->description,
            ));
            */
            $events []= array(
                'title' => $event->title,
                'desc' => $event->description,
            );
        }

        echo json_encode($events);
        Yii::app()->end();
    }
}
