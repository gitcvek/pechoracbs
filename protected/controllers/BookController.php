<?php


  class BookController extends Controller {
    public $urlAlias = 'book';

    public function actionIndex() {

      $criteria = new CDbCriteria();
      $criteria->order = 'date DESC';
      $count = Book::model()->count($criteria);
      $pages = new CPagination($count);

      // results per page
      $pages->pageSize = 10;
      $pages->applyLimit($criteria);
      $models = Book::model()->findAll($criteria);

      if ($count == 0) throw new CHttpException(404);

      $this->render('index', array(
        'models' => $models,
        'pages' => $pages
      ));
    }

    public function actionView($id) {
      $model = $this->loadModelOr404('Book', $id);

      $this->render('view', array(
        'model' => $model
      ));
    }
  }