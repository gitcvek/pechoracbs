<?php

/**
 * Модель для таблицы "pr_historical_events".
 *
 * The followings are the available columns in table 'pr_historical_events':
 * @property integer $id
 * @property string $title
 * @property string $event_date
 */
class HistoricalEvents extends DaActiveRecord {

    const ID_OBJECT = 'project-istoricheskie-sobytiya';

    protected $idObject = self::ID_OBJECT;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Book the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pr_historical_events';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array(
                'title, event_date',
                'required'
            ),
            array(
                'title',
                'length',
                'max' => 255
            ),
            array(
                'event_date',
                'length',
                'max' => 10
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Название',
            'event_date' => 'Дата события',
        );
    }

    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => 'active > 0',
            ),
        );
    }
}
