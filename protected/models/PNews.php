<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 13.11.14
 * Time: 11:32
 */
Yii::import('ygin.modules.news.models.News');
Yii::import('ygin.modules.menu.models.Menu');
class PNews extends News
{
    const MENUID_VIRTULAL_EXHIBITION = 174;
    const MENUCATEGORYID_VIRTULAL_EXHIBITION = 4;
    const MENUID_ABOUT_READING = 172;
    const MENUCATEGORYID_ABOUT_READING = 3;

    public function behaviors()
    {
        $behaviors = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'image',
                'formats' => array(
                    '_list' => array(
                        'width' => 150,
                        'height' => 100,
                    ),
                ),
            ),
        );
        return CMap::mergeArray(parent::behaviors(), $behaviors);
    }

    public function scopes()
    {
        return array(
            'actual' => array(
                'condition' => 'date >= :timeFrom',
                'params' => array(':timeFrom' => strtotime(Yii::app()->params['actual_news_timeout']))),
            'archived' => array(
                'condition' => 'date < :timeBefore',
                'params' => array(':timeBefore' => strtotime(Yii::app()->params['actual_news_timeout']))),
        );
    }


    public function getImage($prefix)
    {
        if ($this->image && file_exists($this->image->getFilePath(true))) {
            return $this->getImagePreview($prefix)->getFilePath();
        }
        return false;
    }

    public function attributeLabels()
    {
        return array(
            'event_date' => 'Event date',
        );
    }

    public function getUrl()
    {
        if ($this->id_news) {
            if (strpos($this->id_news, 'book')) {
                return Yii::app()->createUrl($this->id_news);
            }
            return Yii::app()->createUrl('news/news/view', array('id' => $this->id_news));
        }
        return Yii::app()->createUrl('news/news/index');
    }

    //
    public function afterSave()
    {
        //при сохранениии новости с категорией виртуальные выставки
        //в раздел виртуальные выставки добавляется ссылка
        //id_news_category 3 - pro-чтение 4 - виртуальные выставки
        ///*
        parent::afterSave();
        if ($this->id_news_category == 4 || $this->id_news_category == 3) {
            $findMenuItem = Menu::model()->find('name = :name', array('name' => $this->title));
            if ($findMenuItem) {
                $findMenuItem->delete();
            } else {
            }
            $menu = new Menu;
            if ($this->id_news_category == 4) {
                $menu->id_parent = 174; //menu виртуальные выставки
            }
            if ($this->id_news_category == 3) {
                $menu->id_parent = 172; //menu про-чтение
            }
            //$menu->alias
            $menu->name = $this->title;
            $menu->alias = $this->title;
            $menu->caption = $this->title;
            $menu->content = $this->content;
            $menu->id_module_template = 1;
            $menu->visible = $this->is_visible;
            $menu->external_link = '/news/' . $this->id_news . '/';
            if ($menu->save()) {
                //Yii::log($this->id_news_category.' menu created', CLogger::LEVEL_ERROR, 'category save' );
            } else {
                Yii::log(print_r($menu->getErrors(), true), CLogger::LEVEL_ERROR, 'category save');
            };
        }
        return true;
        //*/
    }

    //
    public function afterDelete()
    {//удалить связи
        parent::afterDelete();//id_news_category 3 - pro-чтение 4 - виртуальные выставки
        if ($this->id_news_category == 4 || $this->id_news_category == 3) {
            $findMenuItem = Menu::model()->find('name = :name', array('name' => $this->title));
            if ($findMenuItem) {
                $findMenuItem->delete();
            } else {

            }
        }
        return true;
    }

    //

    public function getId(){
    return $this->primaryKey;


    }
}
