<?php

  /**
   * Модель для таблицы "pr_book".
   *
   * The followings are the available columns in table 'pr_book':
   * @property integer $id_book
   * @property string $title
   * @property string $short
   * @property string $descr
   * @property integer $image
   * @property string $date
   *
   * The followings are the available model relations:
   * @property File $imageFile
   */
  class Book extends DaActiveRecord implements ISearchable {

    const ID_OBJECT = 'project-kniga';

    protected $idObject = self::ID_OBJECT;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Book the static model class
     */
    public static function model($className = __CLASS__) {
      return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
      return 'pr_book';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
      return array(
        array(
          'title, date',
          'required'
        ),
        array(
          'image',
          'numerical',
          'integerOnly' => true
        ),
        array(
          'title',
          'length',
          'max' => 255
        ),
        array(
          'date',
          'length',
          'max' => 10
        ),
        array(
          'short',
          'safe'
        ),
      );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
      return array(
        'imageFile' => array(
          self::BELONGS_TO,
          'File',
          'image'
        ),
      );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
      return array(
        'id_book' => 'ID',
        'title' => 'Название',
        'short' => 'Краткое описание',
        'descr' => 'Полное описание',
        'image' => 'Обложка',
        'date' => 'Дата',
      );
    }

    public function getImage($preview_prefix = null) {
      if($this->imageFile && file_exists($this->imageFile->getFilePath(true)) && $preview_prefix === null) {
        return $this->imageFile->getFilePath();
      } else if($this->imageFile && file_exists($this->imageFile->getFilePath(true))) {
        return $this->getImagePreview($preview_prefix)->getFilePath();
      }
      return Yii::app()->params['DEFAULT_IMAGE_BOOK'];
    }

    public function behaviors() {
      return array(
        'imagePreviewBehavior' => array(
          'class' => 'ImagePreviewBehavior',
          'imageProperty' => 'imageFile',
          'formats' => array(
            '_index' => array(
              'width' => 100,
              'quality' => 100,
              'resize' => true,
            ),
            '_slider' => array(
              'width' => 80,
              'quality' => 100,
              'resize' => true,
            ),
            '_view' => array(
              'width' => 300,
              'quality' => 100,
              'resize' => true,
            ),
          ),
        ),
      );
    }

    public function getUrl() {
      return Yii::app()->createUrl('book/view', array(
        'id' => $this->primaryKey
      ));
    }

    public function getSearchUrl() {
      return $this->getUrl();
    }

    public function getSearchTitle() {
      return $this->title;
    }
  }