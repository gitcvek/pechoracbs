<?php
  Yii::import('ygin.modules.user.models.User');
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 19.11.14
 * Time: 13:25
 */

class PUser extends User {

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name', 'unique'),
      array('name, user_password, full_name', 'required', 'on' => 'register, backendInsert'),
      array('verifyCode', 'required', 'on' => 'register'),
      array('mail, full_name', 'required', 'on' => 'register, profile, backendInsert, backendUpdate'),
      array('name', 'length', 'max' => 100, 'on' => 'register, backendInsert, backendUpdate'),
      array('name', 'unique', 'on' => 'register, backendInsert, backendUpdate'),
      array('mail', 'length', 'max'=>100),
      array('mail', 'email'),
      array('mail', 'unique', 'on' => 'register, backendInsert'),
      array('mail', 'unique', 'criteria'=>array('condition' => 'id_user <> '.$this->id_user), 'on' => 'profile, backendUpdate'),
      array('user_password', 'length', 'max'=>150, 'min' => 6),
      array('full_name', 'length', 'max'=>200),
      array('verifyCode', 'DaCaptchaValidator', 'caseSensitive' => true, 'on' => 'register'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id_user' => 'Id User',
      'name' => 'Логин',
      'user_password' => 'Пароль',
      'mail' => 'E-mail',
      'full_name' => 'Ф.И.О.',
      'rid' => 'Rid',
      'create_date' => 'Create Date',
      'count_post' => 'Count Post',
      'verifyCode' => 'Код проверки',
      'telephone' => 'Телефон'
    );
  }

}