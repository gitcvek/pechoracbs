<?php

  class DefaultController extends Controller {

    public function actionCheckResults() {
      if (Yii::app()->request->isAjaxRequest) {
        $all_questions = 0;
        $right_question = 0;

        $data = Yii::app()->request->getPost('answers');


        /* update: insert answers stats to db */

       if ($data) {
         foreach ($data as $question => $answer) {
           if (is_array($answer)) {
             foreach($answer as $answerVal) {
               $currentAnswer = DebriefingAnswer::model()->findByPk(array(
                   $answerVal
               ));
               if(!empty($currentAnswer)) {
                 if(!$currentAnswer->count_answers) {
                   $currentAnswer->count_answers = 0;
                 }
                 $currentAnswer->count_answers++;
                 if(!$currentAnswer->update()) {
                   HU::loging('Не могу сохранить данные опроса','/protected/runtime/error_log.log');
                 }
               }
             }
           } else {
             $currentAnswer = DebriefingAnswer::model()->findByPk(array(
                   $answer
             ));
             if(!empty($currentAnswer)) {
               if(!$currentAnswer->count_answers) {
                 $currentAnswer->count_answers = 0;
               }
               $currentAnswer->count_answers++;
               if(!$currentAnswer->update()) {
                 HU::loging('Не могу сохранить данные опроса','/protected/runtime/error_log.log');
               }
             }
           }
         }
       }



        /* end of update */


        if ($data) {
          foreach ($data as $question => $answer) {
            if (is_array($answer)) {
              $_right_answers = DebriefingAnswer::model()->findAll(array(
                'condition' => 'id_debriefing_question = :ID_QUESTION AND right_answer = 1',
                'params' => array(
                  ':ID_QUESTION' => $question
                )
              ));
              $right = true;
              foreach ($_right_answers as $_ra) {
                if (!in_array($_ra->primaryKey, $answer)) {
                  $right = false;
                  break;
                }
              }
              if ($right) {
                $right_question++;
              }
            } else {
              if (DebriefingAnswer::model()->count(array(
                'condition' => 'id_debriefing_question = :ID_QUESTION AND right_answer = 1 AND id_debriefing_answer = :ID_ANSWER',
                'params' => array(
                  ':ID_QUESTION' => $question,
                  ':ID_ANSWER' => $answer
                )
              ))
              ) {
                $right_question++;
              };
            }
            $all_questions++;
          }

          echo CJSON::encode(array(
            'all' => $all_questions,
            'right' => $right_question
          ));

          Yii::app()->end();
        } else {
          echo CJSON::encode(array(
            'all' => 0,
            'right' => 0
          ));

          Yii::app()->end();
        }
      }
      throw new CHttpException(404);
    }
  }