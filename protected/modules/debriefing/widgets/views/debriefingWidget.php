<?php
  /**
   * @var Debriefing $model
   * @var DebriefingQuestion $question
   * @var DebriefingAnswer $answer
   */
  Yii::app()->clientScript->registerScriptFile('/protected/modules/debriefing/widgets/assets/debriefing.js', CClientScript::POS_END);
?>
<div class="b-debriefing js_debriefing">
  <h2 class="title"><?php echo $model->title ?></h2>

  <div class="questions">
    <form>
      <?php if ($model->debriefingQuestions): ?>
        <?php foreach ($model->debriefingQuestions as $question): ?>
          <?php if ($question->debriefingAnswers): ?>
            <div class="question">
              <h4 class="question-title"><?php echo $question->question; ?></h4>
              <ul class="list list-unstyled">
                <?php foreach ($question->debriefingAnswers as $answer): ?>
                  <li>
                    <?php if ($question->multiple_right): ?>
                      <?php echo CHtml::checkBox('answers[' . $question->primaryKey . '][]', false, array(
                        'value' => $answer->primaryKey,
                        'id' => "dbf_answer_" . $question->primaryKey . "_" . $answer->primaryKey
                      )); ?>
                    <?php else: ?>
                      <?php echo CHtml::radioButton('answers[' . $question->primaryKey . ']', false, array(
                        'value' => $answer->primaryKey,
                        'id' => "dbf_answer_" . $question->primaryKey . "_" . $answer->primaryKey
                      )); ?>
                    <?php endif; ?>
                    <?php echo CHtml::label($answer->answer, "dbf_answer_" . $question->primaryKey . "_" . $answer->primaryKey); ?>
                  </li>
                <?php endforeach; ?>
              </ul>
              <?php echo CHtml::htmlButton('Продолжить', array(
                'class' => 'next'
              ));?>
            </div>

          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
      <div class="question">
        <?php echo CHtml::ajaxSubmitButton("Проверить результат", Yii::app()->createUrl('debriefing/default/checkResults/'), array(
          'type' => "post",
          'dataType' => 'json',
          'success' => 'js:function(msg) {
            if(typeof(msg.all) != "undefined") {
              $(".js_debriefing input.js_send").replaceWith("Всего вопросов: "+msg.all+".<br> Правильно отвечено: " + msg.right);
            }
          }'
        ), array(
          'class' => "js_send"
        )); ?>
      </div>
    </form>
  </div>
</div>