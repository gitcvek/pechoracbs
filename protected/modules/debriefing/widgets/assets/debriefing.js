$(".js_debriefing").find(".question:not(:first)").hide();
$(".js_debriefing").find(".question:first").addClass("active");

$(document).on("click",".js_debriefing button.next", function() {
  var $btn = $(this),
      $active = $(".question.active");

    if(val = $active.find("input[name^=answer]:checked").eq(0).val()) {
        $active.hide().removeClass("active").next(".question").addClass("active").fadeIn("fast");
    } else {
        alert("Сначала дайте ответ на текущий вопрос");
    }
});