<?php

class DebriefingWidget extends DaWidget {
	public function run() {
    $cr = new CDbCriteria;
    $cr->condition = "enable_in_widget= 1";
    $cr->with = "debriefingQuestions.debriefingAnswers";
    //$cr->order = "t.sequence";

    $model = Debriefing::model()->find($cr);

    if(null === $model) return false;

    $this->render('debriefingWidget', array(
      'model' => $model
    ));
	}
}
