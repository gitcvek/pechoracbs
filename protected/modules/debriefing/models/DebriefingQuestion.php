<?php

  /**
   * Модель для таблицы "pr_debriefing_question".
   *
   * The followings are the available columns in table 'pr_debriefing_question':
   * @property integer $id_debriefing_question
   * @property string $question
   * @property integer $multiple_right
   * @property integer $sequence
   * @property integer $id_debriefing
   *
   * The followings are the available model relations:
   * @property Debriefing $debriefing
   * @property DebriefingAnswer[] $debriefingAnswers
   */
  class DebriefingQuestion extends DaActiveRecord {

    const ID_OBJECT = 'project-prover-sebya';

    protected $idObject = self::ID_OBJECT;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return DebriefingQuestion the static model class
     */
    public static function model($className = __CLASS__) {
      return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
      return 'pr_debriefing_question';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
      return array(
        array(
          'question, id_debriefing',
          'required'
        ),
        array(
          'multiple_right, sequence, id_debriefing',
          'numerical',
          'integerOnly' => true
        ),
        array(
          'question',
          'length',
          'max' => 255
        ),
      );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
      return array(
        'debriefing' => array(
          self::BELONGS_TO,
          'Debriefing',
          'id_debriefing'
        ),
        'debriefingAnswers' => array(
          self::HAS_MANY,
          'DebriefingAnswer',
          'id_debriefing_question',
          'order' => 'debriefingAnswers.sequence ASC'
        ),
      );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
      return array(
        'id_debriefing_question' => 'ID',
        'question' => 'Вопрос',
        'multiple_right' => 'Несколько правильных ответов',
        'sequence' => 'п/п',
        'id_debriefing' => 'Опрос',
      );
    }
  }