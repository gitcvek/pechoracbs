<?php

/**
 * Модель для таблицы "pr_debriefing_answer".
 *
 * The followings are the available columns in table 'pr_debriefing_answer':
 * @property integer $id_debriefing_answer
 * @property integer $image
 * @property string $answer
 * @property integer $right_answer
 * @property integer $sequence
 * @property integer $id_debriefing_question
 *
 * The followings are the available model relations:
 * @property File $imageFile
 * @property DebriefingQuestion $debriefingQuestion
 */
class DebriefingAnswer extends DaActiveRecord {

  const ID_OBJECT = 'project-prover-sebya---otvet';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return DebriefingAnswer the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_debriefing_answer';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('answer, id_debriefing_question', 'required'),
      array('image, right_answer, sequence, id_debriefing_question', 'numerical', 'integerOnly'=>true),
      array('answer', 'length', 'max'=>255),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'imageFile' => array(self::BELONGS_TO, 'File', 'image'),
      'debriefingQuestion' => array(self::BELONGS_TO, 'DebriefingQuestion', 'id_debriefing_question'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_debriefing_answer' => 'ID',
      'image' => 'Картинка',
      'answer' => 'Ответ',
      'right_answer' => 'Правильный ответ',
      'sequence' => 'п/п',
      'id_debriefing_question' => 'Вопрос',
    );
  }

}