<?php

  /**
   * Модель для таблицы "pr_debriefing".
   *
   * The followings are the available columns in table 'pr_debriefing':
   * @property integer $id_debriefing
   * @property string $title
   * @property integer $active
   * @property integer $enable_in_widget
   *
   * The followings are the available model relations:
   * @property DebriefingQuestion[] $debriefingQuestions
   */
  class Debriefing extends DaActiveRecord {

    const ID_OBJECT = 'project-prover-sebya1';

    protected $idObject = self::ID_OBJECT;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Debriefing the static model class
     */
    public static function model($className = __CLASS__) {
      return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
      return 'pr_debriefing';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
      return array(
        array(
          'title',
          'required'
        ),
        array(
          'active, enable_in_widget',
          'numerical',
          'integerOnly' => true
        ),
        array(
          'title',
          'length',
          'max' => 255
        ),
      );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
      return array(
        'debriefingQuestions' => array(
          self::HAS_MANY,
          'DebriefingQuestion',
          'id_debriefing',
          'order' => 'debriefingQuestions.sequence ASC'
        ),
      );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
      return array(
        'id_debriefing' => 'ID',
        'title' => 'Название опроса',
        'active' => 'Активность',
        'enable_in_widget' => 'Показвать в виджете',
      );
    }

    public function defaultScope() {
      return array(
        'condition' => "active = 1",
      );
    }
  }