<?php /**
 * @var News $model
 *
 */
?>
<!--  <div class="b-page-main-news">-->

<h1>Новости</h1>

<?php foreach ($models as $model): ?>
  <div class="b-page-main-news-item">
    <div
      class="b-page-main-news-item__date"><?php echo Yii::app()->getDateFormatter()->format('dd MMMM yyyy', $model->date) ?></div>

    <div class="b-page-main-news-item__name">
      <?php echo CHtml::link($model->title, $model->getUrl(), array(
        "class" => ""
      )); ?>
    </div>
    <div class="b-page-main-news-item__text">
      <p>
        <?php if ($img = $model->getImage("_list")): ?>
          <?php echo CHtml::image($img, $model->title, array(
            'class' => ''
          ));?>
        <?php endif; ?>
        <?php echo CHtml::encode($model->short); ?>
      </p>
    </div>
  </div>
<?php endforeach; ?>
<div>
  <?php echo CHtml::link('Все новости', Yii::app()->createUrl('/news/news/index/'), array(
    'class' => 'btn btn-default btn-xs pull-right'
  ));?>
</div>
<!--  </div>-->