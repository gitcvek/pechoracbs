<?php /**
 * @var News $model
 *
 */
?>
<div class="b-children-news">

  <h1>Детские новости</h1>

  <?php foreach($models as $model):?>
  <div class="b-children-news-item">
    <div class="b-children-news-item__date"><?php echo Yii::app()->getDateFormatter()->format('dd MMMM yyyy', $model->date)?></div>
    <div class="b-children-news-item__name">
      <?php echo CHtml::link($model->title,$model->getUrl(), array(
        "class" => ""
      )); ?>
    </div>
    <div class="b-children-news-item__text">
      <p>
        <?php echo CHtml::encode($model->short);?>
      </p>
    </div>
  </div>
  <?php endforeach;?>
</div>