<?php
  /**
   * @var Book $model
   */

 Yii::app()->clientScript->registerCssFile(
     Yii::app()->assetManager->publish(
         Yii::getPathOfAlias('application.assets.bxslider').'/jquery.bxslider.css'
     )
 );
  Yii::app()->clientScript->registerScriptFile(
      Yii::app()->assetManager->publish(
          Yii::getPathOfAlias('application.assets.bxslider').'/jquery.bxslider.min.js'
      ),
      CClientScript::POS_END
  );

  Yii::app()->clientScript->registerScript('bx-slider-init',"
    $('.js_bx_slider').bxSlider({
      slideWidth: 100,
      minSlides: 1,
      maxSlides: 6,
      moveSlides: 1,
      slideMargin: 10,
      auto: true,
      pause: 4000,
      autoStart: true,
      controls: false,
    });
  ",CClientScript::POS_READY);
?>

<ul class="book-slider-1 js_bx_slider">
  <?php foreach($models as $model):?>
    <li>
      <?php echo CHtml::link(CHtml::image("/".$model->getImage('_index'),$model->title), $model->getUrl());?>
      <span class="caption">
        <?php echo $model->title?>
      </span>
    </li>
  <?php endforeach;?>
</ul>