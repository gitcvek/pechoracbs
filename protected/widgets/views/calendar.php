<?php Yii::app ()->clientScript->registerScriptFile ('/themes/business/js/responsive-calendar.js',CClientScript::POS_END); ?>
<?php Yii::app ()->clientScript->registerScript ('initCalendar','initCalendar();',CClientScript::POS_END); ?>
<div class="calendar-widget">
    <!-- Responsive calendar - START -->
    <div class="responsive-calendar background-lead" data-control="class" data-class-prefix="responsive-calendar">
        <div class="controls">
            <a class="btn btn-lead darken pull-left" data-go="prev"><i class="fa fa-chevron-left"></i></a>
            <a class="btn btn-lead darken pull-right" data-go="next"><i class="fa fa-chevron-right"></i></a>
            <h4>
                <span data-head-month></span>
                <span data-head-year></span>
            </h4>
        </div>
        <div class="day-headers"><div class="day header">Пн</div><div class="day header">Вт</div><div class="day header">Ср</div><div class="day header">Чт</div><div class="day header">Пт</div><div class="day header">Сб</div><div class="day header">Вс</div>
        </div>

        <div class="days color-white" data-group="days">
            <!-- the place where days will be generated -->
        </div>
    </div>
    <!-- Responsive calendar - END -->
</div>

<div class="modal fade bd-example-modal-lg" id="hEventDetail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<script>
    function initCalendar() {
        $('.responsive-calendar').responsiveCalendar({
            translateMonths:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            time:'<?php echo date('Y-m'); ?>',
            events:{
                <?php foreach ($arrEvents as $date => $item): ?>
                '<?php echo $date; ?>':{
                    "number":<?php echo $item['number']; ?>,
                    "badgeClass":"background-turquoise",
                    "eventsTime":<?php echo $item['events_time']; ?>,
                    "dayEvents":[
                        <?php foreach ($item['events'] as $event): ?>
                        {
                            "id":"<?php echo CHtml::encode($event->id); ?>",
                            "title":"<?php echo CHtml::encode($event->title); ?>",
                        },
                        <?php endforeach; ?>
                    ]
                },
                <?php endforeach; ?>
            },
            onActiveDayClick:function( events ) {
                //console.log(events);
                var month = $ ( this ).data ( 'month' );
                if( month < 10 ) {
                    month = '0' + month;
                }
                var day = $ ( this ).data ( 'day' );
                if( day < 10 ) {
                    day = '0' + day;
                }
                var curDate = $ ( this ).data ( 'year' ) + '-' + month + '-' + day;

                $.ajax({
                    url: '/ajax/events/?event_date=' + events[curDate]['eventsTime'],
                    type: 'GET',
                    async: false,
                    success: function (events) {
                        if (events) {
                            $.each(events, function(k, v) {
                                $('#hEventDetail .modal-body').append('<h3>' + v.title + '</h3>');
                                $('#hEventDetail .modal-body').append('<p>' + v.desc + '</p>');
                            });

                            $('#hEventDetail').modal('toggle');
                        }
                    }
                });

            }
        } );
    }
</script>
