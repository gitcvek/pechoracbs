<?php
 Yii::app()->clientScript->registerScriptFile(
     Yii::app()->assetManager->publish(
         Yii::getPathOfAlias('application.assets').'/calendar/jquery.supercal.js'
     ),
     CClientScript::POS_END
 );

  Yii::app()->clientScript->registerCssFile(
      Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('application.assets').'/calendar/supercal.css'
      )
  );

  Yii::app()->getClientScript()->registerScript('calnder.init',"

    $('.js_calendar_holder').supercal({
      todayButton: true,
      showInput: true,
      weekStart: 1,
      widget: true,
      cellRatio: 1,
      format: 'd/m/y',
      footer: false,
      dayHeader: true,
      mode: 'tiny',
      animDuration: 200,
      transition: '',
      tableClasses: 'table table-condensed',
      hidden: true,
      setOnMonthChange: true,
      condensed: false
    });
  ", CClientScript::POS_END);

?>

<h1>Календарь</h1>
<div class="js_calendar_holder">

</div>