<?php
class MainCalendar extends DaWidget {
    public function run() {
        $allEvents = HistoricalEvents::model()->active()->findAll(array(
                'condition' => 'event_date IS NOT NULL',
                'order' => 'event_date ASC',
            ));

        $arrEvents = array();
        foreach ($allEvents as $event) {
            $date = date('Y-m-d', $event->event_date);

            $arrEvents[$date]['events'][] = $event;
            $arrEvents[$date]['events_time'] = $event->event_date;
        }

        foreach( $arrEvents as $key => $event ) {
            $arrEvents[$key]['number'] = count($arrEvents[$key]['events']);

            $arrEvents[$key][] = array('number' => count($arrEvents[$key]['events']),
                'events_time' => $event->event_date);
        }

        $this->render ('calendar',array(
            'arrEvents' => $arrEvents,
        ));
    }
}
?>

