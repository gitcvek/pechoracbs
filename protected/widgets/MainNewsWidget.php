<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 05.11.14
 * Time: 10:46
 */

class MainNewsWidget extends DaWidget  implements IParametersConfig {
  public $maxNews = 7;
  public $idCategory = 1;

  public static function getParametersConfig() {
    return array(
      'maxNews' => array(
        'type' => DataType::INT,
        'default' => 3,
        'label' => 'Количество отображаемых новостей',
        'required' => true,
      ),
      'idCategory' => array(
        'type' => DataType::INT,
        'default' => 1,
        'label' => 'ИД категории',
        'required' => true,
      ),
    );
  }

  public function run() {

    $news = News::model()->findAll(array(
/*      "condition" => "id_news_category = :ID",
      "params" => array(
        ":ID" => $this->idCategory,
      ),*/
        "condition" => "id_news_category <> :ID",
        "params" => array(
            ":ID" => 2),
      "limit" => $this->maxNews,
      "order" => "date DESC"
    ));

    //добавляем всё из объекта книги
    $books = Book::model()->findAll(array("limit" => $this->maxNews, "order" => "date DESC"));
    foreach ($books as $book) {
      $model = BaseActiveRecord::newModel('PNews');
      $model->title = $book->title;
      $model->date = $book->date;
      $model->short = $book->short;
      $model->photo = $book->image;
      $model->id_news = '/book/'.$book->id_book;
      array_push($news, $model);
    }

    //выбираем последние N новейших
    for ($i=0;$i<count($news)-1;$i++){
      for ($j=$i+1;$j<=count($news)-1;$j++){
          if($news[$i]->date<$news[$j]->date) {
            $buffer = $news[$j];
            $news[$j]=$news[$i];
            $news[$i]=$buffer;
          }
      }
    }
    $newsArray = array();
    for ($i=0;$i<=$this->maxNews-1;$i++) {
      array_push($newsArray,$news[$i]);
    }



    $this->render('mainNews', array(
      'models' => $newsArray
    ));
  }
}