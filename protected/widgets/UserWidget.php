<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 19.11.14
 * Time: 13:03
 */

class UserWidget extends DaWidget {

  public function run() {
    if(Yii::app()->user->isGuest) {
      $this->render('register');
    } else {
      $this->render('profile');
    }
  }

} 