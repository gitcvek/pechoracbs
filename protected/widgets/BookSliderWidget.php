<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 10.11.14
 * Time: 12:21
 */

class BookSliderWidget extends DaWidget implements IParametersConfig {
  public $count = 10;

  public static function getParametersConfig() {
    return array(
      'count' => array(
        'type' => DataType::INT,
        'default' => 10,
        'label' => 'Количество книг в слайдере',
        'required' => true,
      ),
    );
  }

  public function run() {
    $cr = new CDbCriteria;
    $cr->order = "date DESC";
    $cr->limit = $this->count;

    $models = Book::model()->findAll($cr);

    $this->render('bookSlider', array(
      'models' => $models
    ));
  }

}