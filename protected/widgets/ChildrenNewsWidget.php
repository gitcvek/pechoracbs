<?php
/**
 * Created by PhpStorm.
 * User: cranky4
 * Date: 05.11.14
 * Time: 10:46
 */

class ChildrenNewsWidget extends DaWidget {
  const ID_CHILDREN_NEWS_CATEGORY = 2;

  public function run() {

    $news = News::model()->findAll(array(
      "condition" => "id_news_category = :ID",
      "params" => array(
        ":ID" => self::ID_CHILDREN_NEWS_CATEGORY,
      ),
      "limit" => 5,
      "order" => "date DESC"
    ));

    $this->render('childrenNews', array(
      'models' => $news
    ));
  }

} 