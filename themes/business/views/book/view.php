<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 10.11.14
   * Time: 11:46
   *
   * @var Book $model
   */

?>

<div class="b-books">
  <div class="items">
      <div class="item row">
        <div class="book-item col-lg-5">
          <div
            class="pic col-lg-4"><?php echo CHtml::image("/" . $model->getImage("_view"), $model->title, array(
              'class' => 'img umg-thumbnail'
            )); ?></div>
        </div>
        <div class="col-lg-7">
          <div class="title">
            <h3>
              <?php echo $model->title; ?><br>
              <small>
                <?php echo $model->author; ?>
              </small>
            </h3>
          </div>
          <div class="publisher">
            <?php echo $model->publisher?>
          </div>
          <div class="pub-year">
            <?php echo $model->year?>
          </div>
          <div class="short">
            <?php echo $model->short; ?>
            <?php echo $model->descr; ?>
          </div>
        </div>
      </div>
  </div>
</div>