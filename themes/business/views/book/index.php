<?php
  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 10.11.14
   * Time: 11:46
   *
   * @var Book $model
   */

?>

<div class="b-books">
  <div class="items">
    <?php foreach ($models as $model): ?>
      <div class="item row">
        <div class="book-item col-lg-3">
          <div
            class="pic col-lg-4"><?php echo CHtml::link(CHtml::image("/" . $model->getImage("_index"), $model->title, array(
                'class' => 'img umg-thumbnail'
              )), Yii::app()->createUrl('book/view', array('id' => $model->primaryKey))); ?></div>
        </div>
        <div class="col-lg-9">
          <div class="title">
            <h3>
              <?php echo CHtml::link($model->title, Yii::app()->createUrl('book/view', array('id' => $model->primaryKey))); ?>
              <small>
                <?php echo $model->author; ?>
              </small>
            </h3>
          </div>
          <div class="short">
            <?php echo $model->short ?>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="pages">
    <?php $this->widget('LinkPagerWidget', array(
      'pages' => $pages,
    )) ?>
  </div>

</div>