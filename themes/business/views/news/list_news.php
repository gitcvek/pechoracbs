<?php
$this->registerCssFile('news.css');
//print_r($dataProvider);
?>

<div class="b-news-list">
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '/_list_news_item',   // refers to the partial view named '_post'
        'pager' => array(
            'htmlOptions' => array(
                'class' => 'pages',
            ),
            'maxButtonCount'=>'6',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
            'header' => '',
            'cssFile'=>Yii::app()->getBaseUrl(true).'/css/page.css'
        ),
    ));
    ?>
</div>
