<div class="item">
    <div class="date"><?php echo Yii::app()->dateFormatter->format(Yii::app()->getModule('news')->getListDateFormat(), $data->date); ?></div>
    <h3><?php echo CHtml::link($data->title, $data->getUrl()); ?></h3>
    <?php if (($preview = $data->getImagePreview('_list')) !== null): ?>
        <div class="photo">
            <?php echo CHtml::link(CHtml::image($preview->getUrlPath(), $data->title), $data->getUrl(), array('title' => $data->title)); ?>
        </div>
    <?php endif; ?>
    <div class="text"><?php echo nl2br($data->short); ?></div>
    <div class="more"><?php echo CHtml::link('Полный текст', $data->getUrl()); ?> »</div>
</div>