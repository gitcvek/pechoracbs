<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9,chrome=1">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="content-language" content="ru"> <?php // TODO - в будущем генетить автоматом ?>
    <meta name="yandex-verification" content="1c245480b1a5e7bd" />

    <?php $favicon = Yii::getPathOfAlias('webroot.themes.business');
    Yii::app()->clientScript->registerLinkTag('icon', "image/x-icon", Yii::app()->getAssetManager()->publish($favicon) . '/favicon.ico'); ?>

    <?php
    //Регистрируем файлы скриптов в <head>
    if (YII_DEBUG) {
        Yii::app()->assetManager->publish(YII_PATH . '/web/js/source', false, -1, true);
    }

    //Yii::app()->clientScript->registerCoreScript('jquery.project');
    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('bootstrap');
    $bootstrapFont = Yii::getPathOfAlias('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
        $bootstrapFont . 'glyphicons-halflings-regular.eot' => '../fonts/',
        $bootstrapFont . 'glyphicons-halflings-regular.svg' => '../fonts/',
        $bootstrapFont . 'glyphicons-halflings-regular.ttf' => '../fonts/',
        $bootstrapFont . 'glyphicons-halflings-regular.woff' => '../fonts/',
    ));

    Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/business/js/modernizr.custom.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/blind/blind.js', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScript('setScroll', "setAnchor();", CClientScript::POS_READY);
    Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

    Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');
    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!--    <script src="/blind/blind.js"></script>-->
    <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
    <!--[if IE]>
    <link href="/themes/business/css/badbrowser.css" media="screen" rel="stylesheet" type="text/css">
    <![endif]-->
</head>
<body>
<div id="wrap">
    <div id="container">

        <div class="b-top-panel row">

            <div class="b-top-panel__mail">
                <a href="mailto: <?= Yii::app()->params->email_main; ?>"><i
                            class="icon-mail"></i><?= Yii::app()->params->email_main; ?></a>
            </div>

            <div class="b-top-panel__vk">
                <a target="_blank" href="<?= Yii::app()->params->vk_group_main; ?>">
                    <img class="vk-widget" src="/themes/business/gfx/vk_logo.png">
                </a>
            </div>

            <div class="b-top-panel__phone">
                <a href="tel: <?= str_replace(array("-", " ", ")", "("), "", Yii::app()->params->phone_main); ?>"><i
                            class="icon-phone"></i><?= Yii::app()->params->phone_main; ?></a>
            </div>

            <div class="b-top-panel__user">
                <?php $this->widget('application.widgets.UserWidget'); ?>
            </div>

            <div class="b-search-widget">

                <form action="/search/">
                    <input id="query" class="span3" type="text" placeholder="&nbsp;Поиск"
                           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Поиск'" name="query">
                    <input type="submit" value="">
                </form>

            </div>

        </div>
        <div class="b-top-widget-container">
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>
        </div>
        <!---->
        <!--<div class="b-header row">-->
        <!---->
        <!--  <div class="b-header-logo"></div>-->
        <!--  <div class="b-header-arrow-left"><a href="#"><img src="/themes/business/gfx/slide-arrow.png"></a></div>-->
        <!--  <div class="b-header-arrow-right"><a href="#"><img src="/themes/business/gfx/slide-arrow.png"></a></div>-->
        <!--</div>-->

        <?php
        $column1 = 0;
        $column2 = 9;
        $column3 = 0;

        if (Yii::app()->menu->current != null) {
            $column1 = 3;
            $column2 = 5;
            $column3 = 4;

            if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {
                $column1 = 0;
                $column3 = 4;
            }
            if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {
                $column3 = 0;
                //$column1 = $column1 * 4 / 3;
                $column1 = 3;
            }
            $column2 = 12 - $column1 - $column3;
        }
        ?>


        <div class="b-content row">

            <?php if ($column1 > 0): // левая колонка ?>
                <div class="b-left-sidebar col-md-<?php echo $column1; ?>">
                    <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
                </div>
            <?php endif ?>


            <div class="b-page-main-news col-md-<?php echo $column2; ?>">
                <?php if ($this->caption !== null): ?>
                    <div class="page-header"><h1><?php echo $this->caption; ?></h1></div>
                <?php endif; ?>
                <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_CONTENT_TOP)); ?>
                <?php echo $content; ?>
                <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_CONTENT)); ?>
            </div>

            <?php if ($column3 > 0): // левая колонка ?>
                <div id="sidebarRight" class="b-page-right col-md-<?php echo $column3; ?>">
                    <div title='Версия для слабовидящих' class="js-blind-selector">
                        <span class="glyphicon glyphicon-eye-open"></span>
                        Версия для слабовидящих
                    </div>
                    <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
                </div>
            <?php endif ?>

        </div>
        <!-- end b-content -->

        <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>

        <div class="b-footer-wrap">
            <div id="footer" class="container b-footer">
                <div class="row">
                    <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
                    <div class="cvek-logo" onclick="window.open('http://cvek.ru')"></div>
                </div>
                <?php echo Chtml::image('/themes/business/gfx/ajax-loader.gif', 'Загрузка', array(
                    'id' => 'ajaxLoader',
                    'style' => 'display:none;position:fixed;top:50%;left:50%'
                )); ?>
            </div>
        </div>

        <!--<div class="b-footer row">-->
        <!---->
        <!--  <div class="b-footer-copy">-->
        <!--    <p>-->
        <!--      <img src="/themes/business/gfx/logo.png" class="logo-image">-->
        <!--      МБУ <q>Печорская межпоселенческая централизованная библиотечная система</q>-->
        <!--    </p>-->
        <!--  </div>-->
        <!---->
        <!---->
        <!--  -->
        <!---->
        <!--  <div class="b-footer-address">-->
        <!--    Адрес: 169600, Республика Коми, г. Печора, ул. Гагарина, д. 51,<br>-->
        <!--    Центральная бибилиотека<br>-->
        <!--    Телефон: 8(82142)32278-->
        <!--  </div>-->
        <!--</div>-->


    </div>
    <div class="b-feedback-form-link">
        <a href="#" onclick="$('#feedbackForm').modal(); return false;">Вопрос библиотекарю</a>
    </div>
    <div class="b-extend-book-link">
        <a href="/page/levoe_menju.prodlit_knigu/">Продлить книгу</a>
    </div>

    <?php $this->widget('ygin.modules.feedback.widgets.FeedbackWidget'); ?>
    <?php
    if ($this->getId() == 'static') {
        Yii::app()->clientScript->registerScript('menu', '
  /*Добавляем класс активной ссылке в меню*/
var burl=document.location.href;
$.each($(".b-menu-side-list a"),function(){
  if(this.href==burl){$(this).addClass("active-menu-link");};
});
/*Проходим от этого класса к ближайшему ul, вешаем на него expanded*/
var col=$(".active-menu-link").closest("ul").attr("class");
$("a.active-menu-link").parent("li").addClass("active");
$("li.active").parent("ul").addClass("expanded");
/*Раскрываем этот ul*/
col=col.replace(/ /ig,".");
col="."+col+".expanded";
$(col).slideToggle("fast");
  ', CClientScript::POS_READY);
    }
    ?>

    <?php
    /*
      если пользователь не авторизован - не даём ему скачивать книги
     */
    /*  if (Yii::app()->user->isGuest) {
        print_r($this->getId());
        Yii::app()->clientScript->registerScript('download', '
    $.each($("a"),function(){
      if(this.href.match(/.ppt/) || this.href.match(/.pptx/) || this.href.match(/.pdf/)){
      this.href="#";
      $(this).attr("data-toggle","modal");
       $(this).attr("data-target","#dlModal");
      };
    });
      ', CClientScript::POS_READY);
        ?>
        <!-- Modal -->
        <div class="modal fade" id="dlModal" tabindex="-1" role="dialog" aria-labelledby="dlModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="dlModalLabel">Требуется авторизация</h4>
              </div>
              <div class="modal-body">
                Скачивание доступно только зарегистрированным пользователям!
              </div>
            </div>
          </div>
        </div>

    <?php } */ ?>
</body>
</html>
