<?php $this->registerCssFile('vitrine.css'); ?>
<link rel="stylesheet" href="/themes/business/css/page.css">

<div id="ygin-vitrine-carousel" class="b-vitrine carousel slide" data-ride="carousel">

  <!-- Wrapper for slides -->
  <div class="b-header-logo">
    <?php $img = CHtml::image("/themes/business/gfx/header-logo.png","Печорская межпоселенческая централизованная библиотечная система");?>
    <img class="age-limit" src="/themes/business/gfx/agelimit.png" alt="">
    <?php if(Yii::app()->request->getUrl() == "/") {
      echo $img;
    } else {
      echo CHtml::link($img,'/');
    }?>
  </div>
  <div class="carousel-inner">
    <?php
      $i = 0;
      foreach ($models AS $model) {
        $id = $model->id_vitrine;
        $class = ($i == 0) ? 'item active' : 'item';
        $link = $model->link;
        $imgHTML = '';
        if ($model->file) {
          $imgHTML = '<img src="' . $model->file->getPreview(1170,0,"_vitrin")->getUrlPath() . '" alt="' . $model->title . '">';
        } else {
          $imgHTML = '<div class="img"></div>';
        }
        ?>
        <div class="<? echo $class ?>">
          <?php echo $imgHTML ?>
          <div class="carousel-caption">
            <h3 class="title"><a href="<?php echo $link; ?>"><?php echo $model->title; ?></a></h3>

            <div class="text"><?php echo $model->text; ?></div>
          </div>
        </div>
        <?php
        $i++;
      }
    ?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#ygin-vitrine-carousel" data-slide="prev">
    <!--  <span class="glyphicon glyphicon-chevron-left"></span>-->
    <img src="/themes/business/gfx/slide-arrow.png" class="b-header-arrow-left">
  </a>
  <a class="right carousel-control" href="#ygin-vitrine-carousel" data-slide="next">
    <!--  <span class="glyphicon glyphicon-chevron-right"></span>-->
    <img src="/themes/business/gfx/slide-arrow.png" class="b-header-arrow-right">
  </a>
</div>