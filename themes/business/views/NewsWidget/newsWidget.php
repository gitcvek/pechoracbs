<?php
  /**
   * @var News $model
   */
$this->registerCssFile('newsWidget.css');
?>
<!--<div class="b-news-widget">-->
<!--  <h2>Новости</h2>-->
<!--  --><?php
//  //если нужно брать переопределенное представление элемента списка из темы,
//  //то вместо news.views._list_item нужно прописать
//  //webroot.themes.название_темы.views.news._list_item
//  ?>
<?php //foreach ($this->getNews() as $model): ?>
<?php //$this->render('news.views._list_item', array('model' => $model)); ?>
<?php //endforeach; ?>
<!--<div class="archive"><a href="--><?php //echo Yii::app()->createUrl(NewsModule::ROUTE_NEWS_CATEGORY);?><!--">Все новости &nbsp;»</a></div>-->
<!--</div>-->


<h1>Новости</h1>

<div class="b-page-main-news">
  <?php foreach ($this->getNews() as $model): ?>
  <div class="b-page-main-news-item">
    <div class="b-page-main-news-item__date"><?php echo Yii::app()->dateFormatter->format('dd MMMM yyyy',$model->date)?></div>
    <div class="b-page-main-news-item__name"><a href="<?php echo $model->getUrl();?>"><?php echo $model->title?></a></div>
    <div class="b-page-main-news-item__text">
      <p>
        <?php $img = CHtml::image($model->getImagePreview('_list')->getFilePath(), $model->title, array(
          'class' => 'img img-thumbnail',
        ));
        echo Chtml::link($img,$model->getUrl());
        ?>

        <?php echo $model->short;?>
      </p>
    </div>
  </div>
  <?php endforeach; ?>
</div>
