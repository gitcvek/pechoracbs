function makeZoomOfic() {
$('.to_zoom').jqzoom({
  zoomType: 'innerzoom',
  zoomWidth: 300,
  zoomHeight: 250,
  title: false
});
}


function setAnchor() {
  // hide #back-top first
  $("#back-top").hide();
   // fade in #back-top
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('#back-top').fadeIn();
      } else {
        $('#back-top').fadeOut();
      }
    });

    // scroll body to 0px on click
    $('#back-top, #back-top a').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 1500);
      return false;
    });
  });
}

$(function () {
    initBlind({
        needCookieJs: false, // Подключен или нет jQueryCookie. Не обязательно (По умолчанию false - будет подключен автоматом)
        toBlindBtn: '.js-blind-selector' // Кнопка/ссылка включающая режим слабовидящих. Обязательный параметр
    });
});

// левое меню
$(document).ready(function(){
    $(".js_left_menu .sub-item-list").slideUp("fast");

    $(document).on("click",".js_left_menu>.js_toggle_view>a", function() {
        $(this).toggleClass("expanded");
        $(this).siblings(".sub-item-list").slideToggle("fast");
        return false;
    });
});

//пагинатор
$(document).on("click",".pagination li.active a,.pagination li.disabled a,", function() {
    return false;
});
