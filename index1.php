<style>
@mixin visually-hidden ($state: hidden) {

    @if ($state == undo) {
        position: static;
        overflow: visible;
        width:  auto;
        height: auto;
        margin: auto;
        clip:   auto;
    }

    @else {
        border: 0;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
    }

}

/**
 * Accordion component.
 */
.accordion {
    margin:  0;
    margin-bottom: 1.5rem;
    padding: 0;
    list-style: none;
}

    .accordion__item {
        border-bottom: 1px solid #fd665a;

        &:last-of-type {
            border-bottom: none;
        }

    }

        /**
         * The label element which triggers the open/close.
         */
        .accordion__trigger {
            display: block;
            color: #fff;
            font-weight: bold;
            cursor: pointer;
            padding: 1.5rem;
        }

        /**
         * The radio/checkbox element needs to always be hidden.
         */
        .accordion__toggle {
            @include visually-hidden();
        }

        /**
         * Hide the accordion content.
         */
        .accordion__target {
            @include visually-hidden();
            background-color: #fd665a;
            color: #fff;


            /**
             * When the radio/checkbox is checked, show the accordion content.
             */
            .accordion__toggle:checked + & {
                @include visually-hidden(undo);
                padding: 1.5rem;

            }

        }


</style>

<body>
<ul class="accordion">

    <li class="accordion__item">
        <label class="accordion__trigger" for="toggle-05">Consectetur adipiscing</label>
        <input type="checkbox" class="accordion__toggle" id="toggle-05" />
        <div class="accordion__target">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
        </div>
    </li>

    <li class="accordion__item">
        <label class="accordion__trigger" for="toggle-06">Sed do eiusmod</label>
        <input type="checkbox" class="accordion__toggle" id="toggle-06" checked />
        <div class="accordion__target">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
        </div>
    </li>

    <li class="accordion__item">
        <label class="accordion__trigger" for="toggle-07">Tempor incididunt</label>
        <input type="checkbox" class="accordion__toggle" id="toggle-07" />
        <div class="accordion__target">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
        </div>
    </li>

    <li class="accordion__item">
        <label class="accordion__trigger" for="toggle-08">Consectetur adipiscing</label>
        <input type="checkbox" class="accordion__toggle" id="toggle-08" />
        <div class="accordion__target">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
        </div>
    </li>

    <li class="accordion__item">
        <label class="accordion__trigger" for="toggle-09">Sed do eiusmod</label>
        <input type="checkbox" class="accordion__toggle" id="toggle-09" />
        <div class="accordion__target">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
        </div>
    </li>

    <li class="accordion__item">
        <label class="accordion__trigger" for="toggle-10">Tempor incididunt</label>
        <input type="checkbox" class="accordion__toggle" id="toggle-10" />
        <div class="accordion__target">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>
        </div>
    </li>

</ul>
						

</body>